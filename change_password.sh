#!/bin/bash
# Gera os arquivos npmrc, gradle.properties e netrc com as configurações de senha

versao=1.1.0

function main {
    echo "Chave SisBB: "
    read -e username
    echo "Senha SisBB: "
    read -es password
    requireAndroidStudioMemory
    echo ""

    # ------------------------------------------- ATF ----------------------------------------------
    echo O artifactory só está aceitando o nome de usuário em minúsculo. Aplicando o lowercase:
    username=$(echo "$username" | tr '[:upper:]' '[:lower:]')
    echo ""

    echo Criando senha criptografada ATF
    criptoPasswordATF=$(curl -k -u $username:$password -X GET "https://atf.intranet.bb.com.br/artifactory/api/security/encryptedPassword")
    echo Ok!
    echo ""

    # ------------------------------------------- BINARIOS ----------------------------------------------
    echo O binarios só está aceitando o nome de usuário em minúsculo. Aplicando o lowercase:
    username=$(echo "$username" | tr '[:upper:]' '[:lower:]')
    echo ""

    echo Criando senha criptografada
    criptoPasswordBIN=$(curl -k -u $username:$password -X GET "https://binarios.intranet.bb.com.br/artifactory/api/security/encryptedPassword")
    echo ""

    # ------------------------------------------- NPM ----------------------------------------------
    echo Criando senha do npmrc
    curl -k -u $username:$password https://binarios.intranet.bb.com.br/artifactory/api/npm/auth -o ~/.npmrc
    echo timeout=900000 >>~/.npmrc
    curl -k https://binarios.intranet.bb.com.br:443/artifactory/generic-bb-binarios-aic-local/publico/alfred-templates/.npmrc >>~/.npmrc
    echo ""
    # ------------------------------------------- GRADLE ----------------------------------------------
    gradleLocation=~/.gradle/gradle.properties
    echo artifactory_contextUrl=https\://atf.intranet.bb.com.br/artifactory > $gradleLocation
    echo artifactory_bbvirtual=bb-gradle-virtual >> $gradleLocation
    echo artifactory_publish_repokey=bb-gradle-android >> $gradleLocation
    echo artifactory_username=$username >>$gradleLocation
    echo artifactory_password=$criptoPassword >>$gradleLocation
    echo org.gradle.jvmargs=-Xmx${androidStudioMemory}m >>$gradleLocation

    # Retirar esses comentários se quiser usar o proxy
    echo '#systemProp.http.proxyPort=40080' >>$gradleLocation
    echo '#systemProp.https.proxyPort=40080' >>$gradleLocation
    echo '#systemProp.http.proxyHost=localhost' >>$gradleLocation
    echo '#systemProp.https.proxyHost=localhost' >>$gradleLocation
    echo '#systemProp.http.proxyUser=user' >>$gradleLocation
    echo '#systemProp.https.proxyUser=user' >>$gradleLocation
    echo '#systemProp.http.proxyPassword=password' >>$gradleLocation
    echo '#systemProp.https.proxyPassword=password' >>$gradleLocation
    echo '#systemProp.http.nonProxyHosts=https\://dl.google.com/dl/android/maven2/androidx/vectordrawable|https\://dl.google.com|dl.google.com' >>$gradleLocation
    echo '#systemProp.https.nonProxyHosts=https\://dl.google.com/dl/android/maven2/androidx/vectordrawable|https\://dl.google.com|dl.google.com' >>$gradleLocation

    echo 'android.useAndroidX=true' >>$gradleLocation
    echo 'android.enableJetifier=true' >>$gradleLocation
    echo 'android.jetifier.blacklist=pass' >>$gradleLocation
    echo Senha Ajustada em $gradleLocation
    echo ""

    # ------------------------------------------- NETRC ----------------------------------------------
    echo "Configurando o Binarios no NETRC"
    echo machine binarios.intranet.bb.com.br >~/.netrc
    echo login $username >>~/.netrc
    echo password $criptoPasswordBIN >>~/.netrc
    echo Senha Ajustada em ~/.netrc
    chmod 0600 ~/.netrc
    echo ""

    # ------------------------------------------- COCOAPODS ----------------------------------------------
    #echo "Configurando Cocoa Pods"
    #echo ">Removendo cache local cocoapods"
    #rm -rf ~/.cocoapods
    #pod repo-art add cocoapods-bb-local "https://binarios.intranet.bb.com.br/artifactory/api/pods/cocoapods-bb-local"
    #echo "Senha de super usuario da sua máquina"
    #sudo gem install cocoapods-art
}

function requireAndroidStudioMemory() {
    echo "Quanto de memória deseja colocar pro Android Studio? (Exemplos: 2048, 3072, 4096, 8192) "
    read -e androidStudioMemory
}

if [[ $1 == "-v" ]]; then
    echo $versao
elif [[ $1 == "--version" ]]; then
    echo $versao
else
    main
fi
